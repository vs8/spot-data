## This repo contains spot node information captured by Qubole from 07/01/2019 to 07/28/2019 for spot data analysis. We have mapped all data to Amazon AZ IDs for analysis. All data in this file can be joined with cluster_nodes to obtain additional information.

## spot_failure_info_0727.csv
This file contains all spot request allocation failures during the given period with AWS az_ids populated and the appropriate status_code.

## spot_info_0727.csv
This file contains all spot requests made during the given period with AWS az_ids populated.

## spot_loss_info_0727.csv
This file contains information about spot losses for the given period with AWS az_ids populated.


